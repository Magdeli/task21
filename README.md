# Task21

Magdeli Holmøy Asplin

8/28/2019

This program uses Entity Framework on a set of students and a set of supervisors. The user can then add and delete students to and from the list. There is also a button that gives the list of supervisors on JSON form.
