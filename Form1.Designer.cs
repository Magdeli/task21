﻿namespace Task19_EntityFramework
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxSupervisors = new System.Windows.Forms.ComboBox();
            this.txtStudentName = new System.Windows.Forms.TextBox();
            this.btnAddStudent = new System.Windows.Forms.Button();
            this.dgvStudSup = new System.Windows.Forms.DataGridView();
            this.btnShowStud = new System.Windows.Forms.Button();
            this.btnRemoveStud = new System.Windows.Forms.Button();
            this.lblStudName = new System.Windows.Forms.Label();
            this.lblChooseSup = new System.Windows.Forms.Label();
            this.lblRmvStud = new System.Windows.Forms.Label();
            this.btnJSON = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudSup)).BeginInit();
            this.SuspendLayout();
            // 
            // cbxSupervisors
            // 
            this.cbxSupervisors.FormattingEnabled = true;
            this.cbxSupervisors.Location = new System.Drawing.Point(12, 334);
            this.cbxSupervisors.Name = "cbxSupervisors";
            this.cbxSupervisors.Size = new System.Drawing.Size(158, 28);
            this.cbxSupervisors.TabIndex = 0;
            // 
            // txtStudentName
            // 
            this.txtStudentName.Location = new System.Drawing.Point(12, 277);
            this.txtStudentName.Name = "txtStudentName";
            this.txtStudentName.Size = new System.Drawing.Size(158, 26);
            this.txtStudentName.TabIndex = 1;
            this.txtStudentName.TextChanged += new System.EventHandler(this.TxtStudentName_TextChanged);
            // 
            // btnAddStudent
            // 
            this.btnAddStudent.Location = new System.Drawing.Point(12, 381);
            this.btnAddStudent.Name = "btnAddStudent";
            this.btnAddStudent.Size = new System.Drawing.Size(158, 31);
            this.btnAddStudent.TabIndex = 2;
            this.btnAddStudent.Text = "Add student";
            this.btnAddStudent.UseVisualStyleBackColor = true;
            this.btnAddStudent.Click += new System.EventHandler(this.BtnAddStudent_Click);
            // 
            // dgvStudSup
            // 
            this.dgvStudSup.AllowUserToAddRows = false;
            this.dgvStudSup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStudSup.Location = new System.Drawing.Point(54, 12);
            this.dgvStudSup.Name = "dgvStudSup";
            this.dgvStudSup.RowHeadersWidth = 62;
            this.dgvStudSup.RowTemplate.Height = 28;
            this.dgvStudSup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStudSup.Size = new System.Drawing.Size(698, 225);
            this.dgvStudSup.TabIndex = 3;
            this.dgvStudSup.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvStudSup_CellContentClick);
            // 
            // btnShowStud
            // 
            this.btnShowStud.Location = new System.Drawing.Point(591, 334);
            this.btnShowStud.Name = "btnShowStud";
            this.btnShowStud.Size = new System.Drawing.Size(183, 33);
            this.btnShowStud.TabIndex = 4;
            this.btnShowStud.Text = "Show all students";
            this.btnShowStud.UseVisualStyleBackColor = true;
            this.btnShowStud.Click += new System.EventHandler(this.BtnShowStud_Click);
            // 
            // btnRemoveStud
            // 
            this.btnRemoveStud.Location = new System.Drawing.Point(591, 277);
            this.btnRemoveStud.Name = "btnRemoveStud";
            this.btnRemoveStud.Size = new System.Drawing.Size(183, 33);
            this.btnRemoveStud.TabIndex = 5;
            this.btnRemoveStud.Text = "Remove Student";
            this.btnRemoveStud.UseVisualStyleBackColor = true;
            this.btnRemoveStud.Click += new System.EventHandler(this.BtnRemoveStud_Click);
            // 
            // lblStudName
            // 
            this.lblStudName.AutoSize = true;
            this.lblStudName.Location = new System.Drawing.Point(8, 254);
            this.lblStudName.Name = "lblStudName";
            this.lblStudName.Size = new System.Drawing.Size(374, 20);
            this.lblStudName.TabIndex = 6;
            this.lblStudName.Text = "Write the name of the student you would like to add:\r\n";
            this.lblStudName.Click += new System.EventHandler(this.LblStudName_Click);
            // 
            // lblChooseSup
            // 
            this.lblChooseSup.AutoSize = true;
            this.lblChooseSup.Location = new System.Drawing.Point(8, 311);
            this.lblChooseSup.Name = "lblChooseSup";
            this.lblChooseSup.Size = new System.Drawing.Size(281, 20);
            this.lblChooseSup.TabIndex = 7;
            this.lblChooseSup.Text = "Choose the supervisor for this student:\r\n";
            // 
            // lblRmvStud
            // 
            this.lblRmvStud.AutoSize = true;
            this.lblRmvStud.Location = new System.Drawing.Point(392, 254);
            this.lblRmvStud.Name = "lblRmvStud";
            this.lblRmvStud.Size = new System.Drawing.Size(396, 20);
            this.lblRmvStud.TabIndex = 8;
            this.lblRmvStud.Text = "Choose the student you want to remove from the table:";
            // 
            // btnJSON
            // 
            this.btnJSON.Location = new System.Drawing.Point(592, 391);
            this.btnJSON.Name = "btnJSON";
            this.btnJSON.Size = new System.Drawing.Size(182, 29);
            this.btnJSON.TabIndex = 9;
            this.btnJSON.Text = "JSON";
            this.btnJSON.UseVisualStyleBackColor = true;
            this.btnJSON.Click += new System.EventHandler(this.BtnJSON_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnJSON);
            this.Controls.Add(this.lblRmvStud);
            this.Controls.Add(this.lblChooseSup);
            this.Controls.Add(this.lblStudName);
            this.Controls.Add(this.btnRemoveStud);
            this.Controls.Add(this.btnShowStud);
            this.Controls.Add(this.dgvStudSup);
            this.Controls.Add(this.btnAddStudent);
            this.Controls.Add(this.txtStudentName);
            this.Controls.Add(this.cbxSupervisors);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudSup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxSupervisors;
        private System.Windows.Forms.TextBox txtStudentName;
        private System.Windows.Forms.Button btnAddStudent;
        private System.Windows.Forms.DataGridView dgvStudSup;
        private System.Windows.Forms.Button btnShowStud;
        private System.Windows.Forms.Button btnRemoveStud;
        private System.Windows.Forms.Label lblStudName;
        private System.Windows.Forms.Label lblChooseSup;
        private System.Windows.Forms.Label lblRmvStud;
        private System.Windows.Forms.Button btnJSON;
    }
}

