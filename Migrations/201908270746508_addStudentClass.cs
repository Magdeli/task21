namespace Task19_EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addStudentClass : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SupervisorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Supervisors", t => t.SupervisorId, cascadeDelete: true)
                .Index(t => t.SupervisorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "SupervisorId", "dbo.Supervisors");
            DropIndex("dbo.Students", new[] { "SupervisorId" });
            DropTable("dbo.Students");
        }
    }
}
