namespace Task19_EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedDataDescription : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Supervisors (Name) VALUES ('Per')");
            Sql("INSERT INTO Supervisors (Name) VALUES ('Paal')");
            Sql("INSERT INTO Supervisors (Name) VALUES ('Espen')");
        }

        public override void Down()
        {
            Sql("DELETE FROM Supervisors WHERE Name = 'Per'");
            Sql("DELETE FROM Supervisors WHERE Name = 'Paal'");
            Sql("DELETE FROM Supervisors WHERE Name = 'Espen'");
        }
    }
}
