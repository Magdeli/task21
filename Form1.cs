﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task19_EntityFramework.Model;
using Newtonsoft.Json;

namespace Task19_EntityFramework
{
    public partial class Form1 : Form
    {
        string studentName;

        private slNameDBContext slNameContext;
        public Form1()
        {
            InitializeComponent();

            slNameContext = new slNameDBContext();

            // Adding the supervisors to the combobox

            //cbxSupervisors.Items.AddRange(slNameContext.Supervisors.ToArray());
            List<Supervisor> Supervisors = slNameContext.Supervisors.ToList();

            foreach (Supervisor item in Supervisors)
            {
                // Adding each supervisor from the list of supervisors to the supervisorscombobox

                cbxSupervisors.Items.Add(item);
            }

            // Updating the gridview such that the list of students opens automatically
            // when runnning the program

            UpdateGridView();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void TxtStudentName_TextChanged(object sender, EventArgs e)
        {
            // Setting the studentname to be the text written in the TxtStudentName
            // by the user

            studentName = txtStudentName.Text;
        }

        // This button adds a student to the list
        private void BtnAddStudent_Click(object sender, EventArgs e)
        {
            Student student = new Model.Student
            {
                // Creating the new student
                Name = studentName,
                SupervisorId = ((Supervisor)cbxSupervisors.SelectedItem).Id
            };

            slNameContext.Students.Add(student);
            slNameContext.SaveChanges();

            UpdateGridView();
        }

        private void UpdateGridView()
        {
            // Binding the database to the gridview

            BindingSource bindingSource = new BindingSource();
            dgvStudSup.DataSource = bindingSource;

            bindingSource.DataSource = slNameContext.Students.ToList();

            dgvStudSup.Refresh();
        }

        private void DgvStudSup_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnShowStud_Click(object sender, EventArgs e)
        {
            // In this case this button is not necessary, but I included it for 
            // learning purposes as it could be useful in later projects

            UpdateGridView();
        }

        private void BtnRemoveStud_Click(object sender, EventArgs e)
        {
            // Using the selected item from the gridview to delete the student selected

            Student tempStud = slNameContext.Students.Find(dgvStudSup.SelectedRows[0].Cells[0].Value);

            slNameContext.Students.Remove(tempStud);
            slNameContext.SaveChanges();

            UpdateGridView();
        }

        private void LblStudName_Click(object sender, EventArgs e)
        {

        }

        
        private void BtnJSON_Click(object sender, EventArgs e)
        {
            // This button has a messagebox that displays the supervisors in a JSON form
            string JSONString = JsonConvert.SerializeObject(slNameContext.Supervisors.ToList());

            MessageBox.Show(JSONString);
        }
    }
}
