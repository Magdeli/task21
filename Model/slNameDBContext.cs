﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Task19_EntityFramework.Model
{
    class slNameDBContext : DbContext
    {
        public DbSet<Supervisor> Supervisors { get; set; }

        public DbSet<Student> Students { get; set; }

    }
}
